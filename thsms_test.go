package thsms

import (
	"fmt"
	"testing"
)

var apiKey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC90aHNtcy5jb21cL21hbmFnZVwvc2VuZC1zbXNcL2NvbmNsdWRlIiwiaWF0IjoxNjU3MDg4NzQxLCJuYmYiOjE2NTcwODg3NDEsImp0aSI6Ik5UMWx3UTBlcWRvamRzWkoiLCJzdWIiOjEwNjIyMywicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.nGT7Is3yVbEDWLzuLelcnu2oRlceCGxZZ-nNh4cPPng"
var CTX = Init(apiKey)

func TestCheckCredit(t *testing.T) {
	var response Data
	if err := CTX.CheckCredit(&response).Error(); err != nil {
		panic(err)
	}
	fmt.Println(response)
}

func TestSendSMS(t *testing.T) {
	var response DataSendSMS
	message := "MAZOLIC Register OTP=111111 \n(REF=XXXX)"
	tels := []string{"0843546967"}
	if err := CTX.SendSMS(message, tels, &response).Error(); err != nil {
		panic(err.Error())
	}
	fmt.Println(response)
}
