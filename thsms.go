package thsms

import (
	"encoding/json"
	"fmt"
)

type (
	Context interface {
		Error() error
		CheckCredit(response *Data) Context
		SendSMS(message string, msisdn []string, response *DataSendSMS) Context
	}
	context struct {
		apiKey string
		err    error
	}
)

func (c *context) SendSMS(message string, msisdn []string, response *DataSendSMS) Context {
	var apiResponse ResponseSendSMS
	if err := apiSendSMS(c, message, msisdn, &apiResponse); err != nil {
		c.err = err
		return c
	}
	if apiResponse.Code != 200 || apiResponse.Success != true {
		c.err = fmt.Errorf(apiResponse.Message)
		return c
	}
	if err := CopyStructToStruct(apiResponse.Data, response); err != nil {
		c.err = err
		return c
	}
	c.err = nil
	return c
}

func (c *context) CheckCredit(response *Data) Context {
	var apiResponse ResponseCheckCredit
	if err := apiGetCredit(c, &apiResponse); err != nil {
		c.err = err
		return c
	}
	if apiResponse.Code != 200 || apiResponse.Success != true {
		c.err = fmt.Errorf(apiResponse.Message)
		return c
	}
	if err := CopyStructToStruct(apiResponse.Data, response); err != nil {
		c.err = err
		return c
	}
	c.err = nil
	return c
}

func (c *context) Error() error {
	if c.err != nil {
		return c.err
	}
	return nil
}

func CopyStructToStruct(target interface{}, result interface{}) error {
	fooByte, err := json.Marshal(&target)
	if err != nil {
		return err
	}

	err = json.Unmarshal(fooByte, result)
	if err != nil {
		return err
	}
	return nil
}

func PrintStructJson(target interface{}) {
	fooByte, _ := json.MarshalIndent(&target, "", "\t")
	fmt.Println(string(fooByte))
}
