package thsms

type User struct {
	Username  string `json:"username"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Mobile    string `json:"mobile"`
	Email     string `json:"email"`
}

type Wallet struct {
	Credit  string `json:"credit"`
	Balance string `json:"balance"`
}

type Data struct {
	User   User   `json:"user"`
	Wallet Wallet `json:"wallet"`
}

type DataSendSMS struct {
	CreditUsage     int `json:"credit_usage"`
	RemainingCredit int `json:"remaining_credit"`
}

type ResponseCheckCredit struct {
	Success bool   `json:"success"`
	Code    int    `json:"code"`
	Message string `json:"message"`
	Data    Data   `json:"data"`
}

type ResponseSendSMS struct {
	Success bool        `json:"success"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    DataSendSMS `json:"data"`
}
