package thsms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber"
	requests "gitlab.com/trendgolibrary/trend-call"
)

func apiGetCredit(c *context, response *ResponseCheckCredit) error {
	var resRequest requests.Response
	headers := map[string]string{
		fiber.HeaderAuthorization: `Bearer ` + c.apiKey,
	}
	if err := requests.Call().Get(requests.Params{
		URL:     `https://thsms.com/api/me`,
		HEADERS: headers,
		BODY:    nil,
		TIMEOUT: 30,
	}, &resRequest).Error(); err != nil {
		return err
	}
	if resRequest.Code != 200 {
		return fmt.Errorf(string(resRequest.Result))
	}
	if err := json.Unmarshal(resRequest.Result, &response); err != nil {
		return err
	}
	return nil
}

func apiSendSMS(c *context, message string, msisdn []string, response *ResponseSendSMS) error {
	type BodySendMessage struct {
		Sender  string   `json:"sender"`
		Msisdn  []string `json:"msisdn"`
		Message string   `json:"message"`
	}
	body := BodySendMessage{
		Sender:  "SMSOTP",
		Msisdn:  msisdn,
		Message: message,
	}

	bodyForApi, _ := json.Marshal(body)

	var resRequest requests.Response
	headers := map[string]string{
		fiber.HeaderContentType:   "application/json",
		fiber.HeaderAuthorization: `Bearer ` + c.apiKey,
	}
	if err := requests.Call().Post(requests.Params{
		URL:     `https://thsms.com/api/send-sms`,
		HEADERS: headers,
		BODY:    bytes.NewBuffer(bodyForApi),
		TIMEOUT: 30,
	}, &resRequest).Error(); err != nil {
		return err
	}
	if resRequest.Code != 200 {
		return fmt.Errorf(string(resRequest.Result))
	}

	if err := json.Unmarshal(resRequest.Result, &response); err != nil {
		return err
	}
	return nil
}
