module gitlab.com/trendgolibrary/trend-thsms

go 1.17

require (
	github.com/gofiber/fiber v1.14.6
	gitlab.com/trendgolibrary/trend-call v1.0.2
)
